class Client():
    def __init__(self, socket):
        self.socket = socket
        self.name = ""
        self.cameras_output = []
        self.cameras_settings = {}
        self.is_connected = False
        self.emotion_anomaly = 0
        self.emotion_anomaly_value = []
        self.movement_anomaly = 0
        self.persons_anomaly = 0
