import socket               # Import socket module
import _thread
import requests
import cv2
import moviepy.editor as mp
import firebase_admin
from firebase_admin import db
import urllib.request
import os
import shutil
import time
from statistics import mode
from client_model import Client

'''
Common global variables
-hostname: show the TCP server's host name
-rest_api_addr: show the evaluator API URL
-rest_api_endpoint: show the API endpoints
-client_socket: show the shared client socket betwen threads
-client.anomaly: show the current danger level of the survaillance camera
'''
hostname = 'DESKTOP-AHGQ8S2'
rest_api_addr = 'http://127.0.0.1:5000/api/detect/' 
rest_api_endpoint = {'face': 'face', 'emotion': 'emotion', 'movement': 'movement', 'speech': 'speech'}
DATABASE_URL = 'https://allamvizsga-b617a-default-rtdb.firebaseio.com/'
DATABASE_CREDINTIALS_PATH = './serviceAccountKey.json'
connected_clients = {}

def initialize_server():
    ''' Initializes the TCP server

        Creates socket and defines the port, then binds the socket to address.
        Listen enables a server to accept connections.

        Returns
        -------
        server_socket
            a socket which will communicate with clients
        database_instance
            an instance which allows to create CRUD request in all threads
    '''
    # Create a socket object
    server_socket = socket.socket()   
    # Get local machine name      
    host = socket.gethostname() 
    # Reserve a port for your service.
    port = 8080                

    print('Server started!')
    print('Waiting for clients...')

    try:
        # Bind to the port
        server_socket.bind((host, port))        
    except Exception as e:
        print('Error occured at port binding: ', e)

    #estabilish connection with Firebase
    cred_obj = firebase_admin.credentials.Certificate(DATABASE_CREDINTIALS_PATH)
    try: 
        database = firebase_admin.initialize_app(cred_obj, {
        'databaseURL':DATABASE_URL
        })
        print('[INFO]', 'Firebase connection ready!')
    except Exception as e:
        print('[ERROR]', 'Could not connect to databse: ', e)
        return

    # Now wait for client connection.
    server_socket.listen(5)       
    return server_socket, database


def on_new_client(client, addr, connected_clients):
    ''' On new client connection is called. Connects the survailance camera stream to client
    Parameters:
    -----------
    client: Client
        client reference
    addr: _RetAddress
        the address of client for further actions
    connected_clients
        all connected clients list
    '''

    #Get the first 2 byte from client ID message
    msg_length = client.socket.recv(2)
    if msg_length != None:
        #Extract client name
        msg = client.socket.recv(int(str(msg_length)[2:-1]))
        name = str(msg)    
        client.name = name[3:-1]
        
        #Check if client was connected or not
        if client.name not in connected_clients:
            client.is_connected = True
            connected_clients[client.name] = client
            send_notification(client, 'Welcome to the server: ' 
                + str(client.name))
        elif connected_clients[client.name].is_connected == False:
            connected_clients[client.name].socket = client.socket
            connected_clients[client.name].is_connected = True

            send_notification(client, 'Welcome back: ' + 
                str(client.name))

            #Check if there is a current anomalie since disconnecting
            if connected_clients[client.name].emotion_anomaly > 80:
                send_notification(client, str(mode(client.emotion_anomaly_value)))
            elif connected_clients[client.name].movement_anomaly > 80:
                send_notification(client, 'VIOLENCE')
            elif connected_clients[client.name].persons_anomaly > 80:
                send_notification(client, 'UNKNOWN')
        else:
            print('[WARNING]', client.name, '--User already connected!')
            return
    print('[INFO] Got connection from', addr, '   |    Client id:', client.name)

    #Starting the camera stream processing
    _thread.start_new_thread(listen_client, (connected_clients, client.name))
    time.sleep(0.2)
    _thread.start_new_thread(process_client_data, (connected_clients, client.name))


def listen_client(connected_clients, client_name):
    ''' Start client listening by waiting for messages
        Check at every iteration if the client is stil connected.
        Wait for incoming messages. Only one type of message is treated:
        'NEW_CAMERA'. After this message a new vide process thread will start
        analizing the new camera output.

        Parameters:
        -----------
        connected_clients: Client
            all connected clients list
        client_name: String
            The name of client reference 
    '''

    client = connected_clients[client_name]
    print('[INFO]', client.name, '--Client stream listening started...')

    while True:
        if is_client_disconnected(connected_clients, client_name) == False:
            print('[WARNING]', client_name, 
                '--Client is offline, message listening stopped!')
            return

        try:
            msg_length = client.socket.recv(2)
            if msg_length is not None:
                msg = str(client.socket.recv(int(str(msg_length)[2:-1])))
                msg = msg[3:-1]

            if msg is not None and len(msg) > 0:
                print('[INFO]', client.name, '--Message arrived:', msg)
                if msg == 'NEW_CAMERA':
                    print('[INFO]', client.name, '--New camera input observed!')
                    process_video_input(connected_clients, client.name, database)
        except:
            connected_clients[client_name].is_connected = False
            print('[WARNING]', client_name, 
                '--Client is offline, message listening stopped!')
            return


def is_client_disconnected(connected_clients, client_name) -> bool:
    ''' Checks if the client with given name in parameter is stil connected
        to the server or not.

        Parameters:
        -----------
        connected_clients: Client
            all connected clients list
        client_name: String
            The name of client reference 

        Returns:
        -----------
        bool value
            it's value is based on wether a client is connected or not
    '''
    client = connected_clients[client_name]

    try:
        # this will try to read bytes without blocking and also without removing them from buffer (peek only)
        data = client.socket.send(str.encode('check status'))
        if data > 0:
            return True
    except:
        connected_clients[client_name].is_connected = False
        connected_clients[client_name].cameras_output = []
        return False
    return False


def get_cameras(client):
    ''' Retrieves all cameras from database for a given client.

        Parameters:
        -----------
        client: Client
            client reference

        Returns:
        -----------
        cameras
            data retrieved from database which consist a list of link to cameras output
    '''

    print('[INFO]', client.name, '--Getting camera records...')
    ref = db.reference('/camera/' + str(client.name))

    if ref == None:
        print('[ERROR]', client.name, 
            '--Could not get camera database endpoint!')

    #send database GET request
    cameras = ref.get()
    if cameras != None:
        if type(cameras) is dict:
            cameras = list(cameras.values())
        else: 
            cameras.pop(0)

    print('[INFO]', client.name, '--Succesfully got cameras from database!')
    return cameras


def get_camera_settings(client):
    ''' Retrieves all cameras settings from database for a given client.

        Parameters:
        -----------
        client: Client
            client reference

        Returns:
        -----------
        settings
            data retrieved from database which consist a dictionary with camera settings
    '''

    print('[INFO]', client.name, '--Getting cameras settings...')
    ref = db.reference('/settings/' + str(client.name))

    if ref == None:
        print('[ERROR]', client.name, 
            '--Could not get cameras settings endpoint!')

    #sending database GET request
    settings = ref.get()
    if settings is not None:
        settings.pop(0)
    print('[INFO]', client.name, 
        '--Succesfully got cameras settings from database!')
    return settings


def get_allowed_persons(client):
    ''' Retrieves all humans in whitelist for a given client.

        Parameters:
        -----------
        client: Client
            client reference

        Returns:
        -----------
        persons
            data retrieved from database which consist all persons names and profile pictures
    '''

    print('[INFO]', client.name, '--Getting user profiles...')
    path = 'shared_sources/client' + client.name + '/profiles/'
    ref = db.reference('/profile/' + str(client.name))

    if ref == None:
        print('[ERROR]', client.name, 
            '--Could not get user profiles!')

    #send database GET request
    persons = ref.get()

    if persons != None:
        if type(persons) is dict:
            persons = list(persons.values())
        else: 
            persons.pop(0)
        
        #saving profile pictures
        if not os.path.exists(path):
            os.makedirs(path)
        for person in persons:
            urllib.request.urlretrieve(person['pictureURL'], 
                path + person['personName'] + '.jpg')

        print('[INFO]', client.name, 
            '--Succesfully got user profiles from database!') 
    return persons


def send_notification(client, notification):
    ''' Sends alert message to the client
    Parameters:
    -----------
    client: Client
        client reference
    notification: String
        the message
    '''

    if is_client_disconnected(connected_clients, client.name):
        print('[INFO]', client.name, '--Sending message to client...')
        
        #building the message
        msg = notification + '\n'
        msg_length = len(msg) + 1
        if msg_length > len(msg):
            msg_length += 1
        msg = str(msg_length) + '/' + notification

        try:
            byteSent = client.socket.send(str.encode(msg))
        except ConnectionResetError:
            print('[WARNING]', client.name, 
                '--Client is offline, message sending aborted!')
            return

        #if message could not be sent retrying til can be sent
        while byteSent < 0:
            print('[WARNING]', client.name, 
                '--Notification could not be sent. Retrying...')
            try:
                byteSent = client.socket.send(str.encode(msg))
            except ConnectionResetError:
                print('[WARNING]', client.name, 
                    '--Client is offline, message sending aborted!')
                return
        print('[INFO]', client.name, '--Message sent!')


def check_cameras_settings(client, nth_camera):
    ''' Checks if there is any enabled option for camera AI

        Parameters:
        -----------
        client: Client
            client reference
        nth_camera: Int
            the number of the camera

        Returns:
        -----------
        bool value
            returns true if there is at least on option enabled
    '''

    if nth_camera < len(client.cameras_settings):
        settings = client.cameras_settings[nth_camera]
        if settings['emotion'] == 'False' \
            and settings['face'] == 'False' \
            and settings['violence'] == 'False' \
            and client.settings['voice'] == 'False':
            return False
    return True


def process_client_data(connected_clients, client_name):
    ''' Collects data about client: cameras, cameras settings, profiles.
        Starts processing every camera output in new thread.

    Parameters:
    -----------
    connected_clients: Dict
        all connected clients dict
    client_name: String
        client name reference
    '''

    client = connected_clients[client_name]
    cameras = get_cameras(client)

    if cameras is None:
        print('[WARNING]', client.name, '--No camera video source found!')
        return
    else:
        cameras_settings = get_camera_settings(client)
        get_allowed_persons(client)

    if cameras_settings is None:
        print('[WARNING]', client.name, 
            '--No camera setting found! Default settings is being used!')
        cameras_settings = {'emotion': True, 'face': True, 'violence': True, 'voice': True}

    connected_clients[client_name].cameras_settings = cameras_settings
    client = connected_clients[client_name]

    #Video playback starting
    print('[INFO]', client.name, '--Video processing...')
    exist_new_camera = False
    for nth_cam, camera in enumerate(cameras):
        if check_cameras_settings(client, nth_cam) == False:
            continue
        if camera != None:
            if camera not in client.cameras_output:
                exist_new_camera = True
                _thread.start_new_thread(process_video_input, (connected_clients, client.name, camera, nth_cam))
    if exist_new_camera == False:
       print('[INFO]', client.name, '--Video processing succesfully ended, there is no new camera!')


def process_video_input(connected_clients, client_name, camera, nth_cam):
    ''' Processes a single video input. First of all check if the client is still connected.
        Download the camera output, extracts the images of video.
        Sends API request with every 5th and 10th image.
        After processing the images the downloaded files will be deleted.

        Parameters:
        -----------
        connected_clients: Dict
            all connected client reference
        client_name: String
            client name reference
        camera: String
            camera acces point
        nth_cam: Int
            the number of the camera
    '''

    connected_clients[client_name].cameras_output.append(camera)
    client = connected_clients[client_name]

    if not is_client_disconnected(connected_clients, client_name):
        print('[INFO', client_name, 
            '--Client is offline, video processing stopped for camera: ' + str(nth_cam) + '.avi' + '!')
        return

    path = 'shared_sources/client' + client.name + '/'
    video_file = path + 'cam' + str(len(client.cameras_output) - 1) + '.avi'
    audio_file = path + 'cam' + str(len(client.cameras_output) - 1) + '.mp3'

    if not os.path.exists(path):
        os.makedirs(path)

    urllib.request.urlretrieve(camera, video_file) 

    if client.cameras_settings[nth_cam]['voice'] == True:
        #Audio extraction
        sound = mp.VideoFileClip(video_file)
        if sound.audio != None:
            if not os.path.exists(path):
                os.makedirs(path)
            sound.audio.write_audiofile(audio_file)
            send_request_to_api(connected_clients, client.name, 
                rest_api_endpoint['speech'], data = audio_file)
    
    #send_request_to_api(connected_clients, client.name, rest_api_endpoint['speech'], data = 'test.wav')
    
    cap = cv2.VideoCapture(video_file) 
    nth_frame = 0
    while cap.isOpened():
        #Reading frames
        ret, frame = cap.read()

        extracted_image_path = path + 'frames/'
        extracted_image_file = extracted_image_path + str(nth_frame) + '.jpg'
        if not os.path.exists(extracted_image_path):
            os.makedirs(extracted_image_path)

        if ret == False:
            break

        rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2BGR)

        #Saving the extracted frame
        cv2.imwrite(extracted_image_file, frame)
        if os.path.exists(extracted_image_file):
            nth_frame += 1

        if nth_frame % 10 == 0:
            #Request sending with saved frame
            if client.cameras_settings[nth_cam]['emotion'] == True:
                send_request_to_api(connected_clients, client.name, 
                    rest_api_endpoint['emotion'], data = extracted_image_file)
            if client.cameras_settings[nth_cam]['face'] == True:
                send_request_to_api(connected_clients, client.name, 
                    rest_api_endpoint['face'], data = extracted_image_file)
        if nth_frame % 5 == 0:
            if client.cameras_settings[nth_cam]['violence'] == True:
                send_request_to_api(connected_clients, client.name, 
                    rest_api_endpoint['movement'], data = extracted_image_file)
    if os.path.exists(path + 'frames'):
        try:
            shutil.rmtree(path + 'frames')
        except:
            print('[WARNING]', client.name, '--Extracted frames could not be freed up!')
    print('[INFO]', client.name, '--Video processing succesfully ended for camera:' + str(nth_cam) + '.avi' + '!')


def send_request_to_api(connected_clients, client_name, endpoint_name, data):
    ''' Send a request to API and processes the response
    Parameters:
    -----------
    connected_clients: Dict
        all connected clients reference
    client_name: string
        Client`s name reference
    endpoint_name: string
        which endpoint should be evaluated
    data: String
        data acces point which should be evaluated
    '''

    print('[INFO]', 'Sending HTTP ' + endpoint_name + ' request to API...')

    #Openening the saved frames
    if endpoint_name == 'speech':
        try:
            files = {'speech': open(data, 'rb')}
        except Exception as e:
            print('[ERROR]', 'Could not open the frame: ', e)
            return
    elif endpoint_name == 'face':
        path = 'shared_sources/client' + client_name + '/profiles/'
        files = []
        for profiles_images in os.listdir(path):
            try:
                files.append(('image', (open(path + profiles_images, 'rb'))))
            except Exception as e:
                print('[ERROR]', 'Could not open the frame: ', e)
                break
        try:
            files.append(('image', (open(data, 'rb'))))
        except Exception as e:
            print('[ERROR]', 'Could not open the frame: ', e)
            return
    else:
        try:
            files = {'image': open(data, 'rb')}
        except Exception as e:
            print('[ERROR]', 'Could not open the frame: ', e)
            return

    try:
        # sending post request and saving response as response object
        r = requests.post(rest_api_addr + endpoint_name, files = files)
        r.raise_for_status()
        response = r.json()
        print(response)
    except Exception as e:
        print('[ERROR]', e)
        return

    #Checking the respone status: empty / not empty
    if "state" in response and bool(response['state']):
        #Depending on endpoint type and response state new client.anomaly will be calculated
        if endpoint_name == 'emotion':
            if response['state'] == 'angry' or response['state'] == 'fear':
                calculate_anomaly( connected_clients, client_name, 
                    endpoint_name, response['state'])
            else:
                calculate_anomaly( connected_clients, client_name, 
                    endpoint_name, 'positive')
        elif endpoint_name == 'speech':
            if response['state'] == 'angry' or response['state'] == 'fear':
                calculate_anomaly( connected_clients, client_name, 
                    endpoint_name, response['state'])
            else:
                calculate_anomaly( connected_clients, client_name, 
                    endpoint_name, 'positive')
        elif endpoint_name == 'movement':
            if response['state'] == 'kick' or response['state'] == 'punch':
                calculate_anomaly( connected_clients, client_name, 
                    endpoint_name, 'fight')
            else:
                calculate_anomaly( connected_clients, client_name, 
                    endpoint_name, 'nonfight')
    elif "persons" in response and bool(response['persons']):
        if 'unknown' in response['persons']:
            calculate_anomaly( connected_clients, client_name, 
                endpoint_name, 'unknown')
        else:
            calculate_anomaly( connected_clients, client_name, 
                endpoint_name, 'known')


def calculate_anomaly(connected_clients, client_name, input_type, activity):
    ''' Calculates the new client anomalies
    Parameters:
    -----------
    connected_clients: Dict
        all connected clients reference
    client_name: String
        client name reference
    input_type: String
        reference to the current process type
    activity: String
        stores the type of evalutaion result
    '''

    print('Calculating client.anomaly...')
    if input_type == 'emotion':
        if activity == 'positive' and \
            connected_clients[client_name].emotion_anomaly > 9:
            connected_clients[client_name].emotion_anomaly -= 5
        elif activity == 'negative' and \
            connected_clients[client_name].emotion_anomaly < 91:
            connected_clients[client_name].emotion_anomaly += 5
            connected_clients[client_name].emotion_anomaly_value.append(activity.upper())
    elif input_type == 'face':
        if activity == 'known' and \
            connected_clients[client_name].persons_anomaly > 9:
            connected_clients[client_name].persons_anomaly -= 5
        elif activity == 'unknown' and \
            connected_clients[client_name].persons_anomaly < 91:
            connected_clients[client_name].persons_anomaly += 5
    if input_type == 'speech':
        if activity == 'positive' and \
            connected_clients[client_name].emotion_anomaly > 9:
            connected_clients[client_name].emotion_anomaly -= 5
        elif activity == 'negative' and \
            connected_clients[client_name].emotion_anomaly < 91:
            connected_clients[client_name].emotion_anomaly += 5
            connected_clients[client_name].emotion_anomaly_value.append(activity.upper())
    elif input_type == 'movement':
        if activity == 'nonfight' and \
        connected_clients[client_name].movement_anomaly > 9:
            connected_clients[client_name].movement_anomaly -= 10
        elif activity == 'fight' and \
            connected_clients[client_name].movement_anomaly < 91:
            connected_clients[client_name].movement_anomaly += 10
            
    check_anomaly(connected_clients, client_name)


def check_anomaly(connected_clients, client_name):
    ''' Checks if the current client`s anomalies are higher then the thresholds.
        If there is higher than the threshold an alert message will be sent.
    Parameters:
    -----------
    connected_clients: Dict
        all connected clients reference
    client_name: String
        client name reference
    '''

    client = connected_clients[client_name]
    
    print('[INFO]', client_name,  '--Anomaly levels: ', 
        client.emotion_anomaly, '%  |  ', client.movement_anomaly, 
        '%  |  ', client.persons_anomaly, '%')

    if client.emotion_anomaly >= 80:
        send_notification(client, str(mode(client.emotion_anomaly_value)))
    elif connected_clients[client.name].movement_anomaly >= 80:
        send_notification(client, 'VIOLENCE')
    elif connected_clients[client.name].persons_anomaly >= 80:
        send_notification(client, 'UNKNOWN')
    

if __name__ == '__main__':
    server_socket, database = initialize_server()
    while True:
        try:
            # Establish connection with client.
            client_socket, addr = server_socket.accept()
        except Exception as e:
            print('Error occured at accepting the client: ', e)
        client = Client(client_socket)
        _thread.start_new_thread(on_new_client, 
            (client, addr, connected_clients))
    server_socket.close()